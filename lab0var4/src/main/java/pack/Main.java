package pack;

public class Main {

    public double beginTask(double d)
    {
        assert d > 0 : "Argument should be more than zero";
        return d*3.14;
    }

    public int integerNumbersTask(int a, int b)
    {
        assert a > 0 && b > 0 : "Arguments should be more than zero";
        return a/b;
    }

    public boolean booleanTask(int a, int b) {

        return a>2 && b<=3;
    }

    public int ifTask(int a, int b, int c)
    {
        int num = 0;
        if(a > 0)
            num++;
        if(b > 0)
            num++;
        if(c > 0)
            num++;
        return num;
    }

    public int switchTask(int number) {
        assert number > 0 && number < 12 : "Argument should be bigger then 0 and less then 12";
        switch (number){
            case 1: case 3: case 5: case 7: case 9: case 11: return 31;
            case 2: return 28;
            case 4: case 6: case 8: case 10: case 12: return 30;
            default: return 0;
        }
    }

    public double forTask(double x, int n)
    {
        assert n > 0 : "Argument should be more than zero";
        double sum = 1;
        double stepX = x*x;
        long fact = 2;
        int minus = -1;

        for (int i = 2; i<n*2; i+=2)
        {
            sum+= minus *stepX/fact;
            stepX *= x*x;
            fact *= (i-1)*i ;
            minus *= (-1);
        }
        return sum;
    }


    /**
     *
     * @param n
     * @return
     */
    public boolean whileTask(int n)
    {
        assert n > 0 : "Argument should be more than zero";
        int tmp = 1;
        while(tmp < n)
            tmp*=3;
        return tmp == n;
    }

    public double arrayTask(double[] array, int k, int l)
    {
        assert (k<l && l<array.length && k<array.length) : "K must be less then l and l must be less then length of array";
        double sum=0, num = 0;
        for(int i = k; i<=l; i++)
        {
            sum+= array[i];
            num++;
        }
        return sum/num;
    }

    public int[][] twoDimensionArrayTask(int[][] arr) {
        int max = 0, min = 0, maxIndex, minIndex, tmp;

        for (int i = 0; i < arr[1].length; i++) {
            max = arr[0][i];
            maxIndex = 0;
            min = arr[0][i];
            minIndex = 0;
            for (int j = 0; j < arr.length; j++) {
                if (arr[j][i] > max) {
                    max = arr[j][i];
                    maxIndex = j;
                }
                if (arr[j][i] < min) {
                    min = arr[j][i];
                    minIndex = j;
                }
            }
            tmp = arr[maxIndex][i];
            arr[maxIndex][i] = arr[minIndex][i];
            arr[minIndex][i] = tmp;
        }
        return arr;
    }
}
