package model;
import static org.testng.Assert.assertEquals;
import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;;

public class MyTest {
    @Test
    public void getFullNameCheck()
    {
        Applicant appl = new Applicant("testFirst", "testLast", "testSurname",
                "KT941245", "MO842212", new Exams(145, 156, 184),
                new Speciality("testUniv", "testFac", "testSpec"), 10.1, true);
        String str = appl.getFirstName() + " " + appl.getLastName() + " " + appl.getSurname();
        assertEquals(str, "testFirst testLast testSurname");
    }

    @Test
    public void checkSetterPassCode()
    {
        Applicant appl = new Applicant("testFirst", "testLast", "testSurname",
                "KT941245", "MO842212", new Exams(145, 156, 184),
                new Speciality("testUniv", "testFac", "testSpec"), 10.1, true);
        appl.newBuilder().setPassportCode("test123456");
        assertEquals(appl.getPassportCode(), "test123456");
    }

    @Test(dataProvider = "getIntroductoryAssessmentProvider")
    public void getIntroductoryAssessment(double averangeMarkOfTheCertificate, boolean ruralCoef, String speciality, double res)
    {
        assertEquals(new Exams(150, 150, 150).getIntroductoryAssessment(averangeMarkOfTheCertificate, ruralCoef, speciality), res);
    }

    @DataProvider
    public Object[][] getIntroductoryAssessmentProvider()
    {
        return new Object[][]{{11, false, "test", 146.5}, {10.5, true, "Біологія", 156.366}};
    }

    @Test (expectedExceptions = AssertionError.class)
    public void checkAssessmentException()
    {
        new Exams(150, 160, 170).getIntroductoryAssessment(-1, true, "asd");
    }

    @Test(dataProvider = "equalsProvider")
    public void equalsTest(Applicant appl1, Applicant appl2, boolean res)
    {
        assertEquals(appl1.equals(appl2), res);
    }

    @DataProvider
    public Object[][] equalsProvider()
    {
        Applicant appl1 = new Applicant("testFirst1", "testLast1", "testSurname1",
                "KT941245", "MO842212",
                new Exams(145, 156, 184),
                new Speciality("testUniv1", "testFac1", "testSpec1"), 10.1, true);
        Applicant appl2 = new Applicant("testFirst2", "testLast2", "testSurname2",  "KT941245", "MO842212",
                new Exams(145, 156, 184),
                new Speciality("testUniv2", "testFac2", "testSpec2"), 10.1, true);

        Applicant appl3 = new Applicant("testFirst1", "testLast1", "testSurname1",
                "KT941245", "MO842212",
                new Exams(145, 156, 184),
                new Speciality("testUniv1", "testFac1", "testSpec1"), 10.1, true);

        return new Object[][] { {appl1, appl2, false}, {appl1, appl3, true}};
    }

    @Test(dataProvider = "hashCodeProvider")
    public void hashCodeTest(Applicant appl, int res)
    {
        assertEquals(appl.hashCode(), res);
    }

    @DataProvider
    public Object[][] hashCodeProvider()
    {
        Applicant appl1 = new Applicant("testFirst1", "testLast1", "testSurname1",
                "KT941245", "MO842212",
                new Exams(145, 156, 184),
                new Speciality("testUniv1", "testFac1", "testSpec1"), 10.1, true);

        return new Object[][] { {appl1, -793574187}};
    }

    @Test(dataProvider = "toStringProvider")
    public void toStringTest(Applicant appl, String  res)
    {
        assertEquals(appl.toString(), res);
    }

    @DataProvider
    public Object[][] toStringProvider()
    {
        Applicant appl1 = new Applicant("testFirst1", "testLast1", "testSurname1",
                "KT941245", "MO842212",
                new Exams(145, 156, 184),
                new Speciality("testUniv1", "testFac1", "testSpec1"), 10.1, true);

        return new Object[][] { {appl1, "145 156 184"}};
    }
}