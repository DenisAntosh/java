import ioservice.*;
import model.*;

import javax.xml.bind.JAXBException;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        System.out.println("JSON SERIALIZE");
        String firstName = "Testfirst";
        String lastName = "Testlast";
        String surname = "Testsurname";
        String passportCode = "KT941245";
        String certificateCode = "KT956745";
        String firstExam = "100";
        String secondExam = "166";
        String thirdExam = "194";
        String university = "testUniv";
        String faculty = "testFac";
        String speciality = "testSpec";
        String averangeMarkOfTheCertificate = "11.9";
        String ruralCoef = "true";

        Serializer jsonSerialize = new JsonSerialize();
        String path = "ser." + "json";
        Applicant appl2 = Applicant.createApplicant();
        String[] applicant = {firstName, lastName, surname, passportCode, certificateCode, firstExam, secondExam,
                              thirdExam, university, faculty, speciality, averangeMarkOfTheCertificate, ruralCoef
        };
        Applicant appl1 = new Applicant.Builder().buildApplicant(applicant);
        try {
            jsonSerialize.serialize(appl1, path);
            appl2 = jsonSerialize.deserialize(path);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        System.out.println(appl2.getFirstName() + " " + appl2.getLastName() + " " + appl2.getSurname());
        System.out.println(appl2.getPassportCode() + " " + appl2.getCertificateCode());
        System.out.println("EXAM");
        System.out.println(appl2.getExam().getFirstExam() + " " + appl2.getExam()
                .getSecondExam() + " " + appl2.getExam().getThirdExam());
        System.out.println("SPECIALITY");
        System.out.println(appl2.getSpeciality().getUniversity() + " " + appl2.getSpeciality()
                .getFaculty() + " " + appl2.getSpeciality().getSpeciality());
        System.out.println(appl2.getAverangeMarkOfTheCertificate() + " " + appl2.getRuralCoef());
        System.out.println("\n");

        System.out.println("XML SERIAIZE");
        Serializer xmlSerialize = new XmlSerialize();
        path = "ser." + "xml";
        try {
            xmlSerialize.serialize(appl1, path);
            appl2 = xmlSerialize.deserialize(path);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        System.out.println(appl2.getFirstName() + " " + appl2.getLastName() + " " + appl2.getSurname());
        System.out.println(appl2.getPassportCode() + " " + appl2.getCertificateCode());
        System.out.println("EXAM");
        System.out.println(appl2.getExam().getFirstExam() + " " + appl2.getExam()
                .getSecondExam() + " " + appl2.getExam().getThirdExam());
        System.out.println("SPECIALITY");
        System.out.println(appl2.getSpeciality().getUniversity() + " " + appl2.getSpeciality()
                .getFaculty() + " " + appl2.getSpeciality().getSpeciality());
        System.out.println(appl2.getAverangeMarkOfTheCertificate() + " " + appl2.getRuralCoef());
        System.out.println("\n");

        System.out.println("TXT SERIALIZE");
        Serializer txtSerialize = new TxtSerialize();
        path = "ser." + "json";
        try {
            txtSerialize.serialize(appl1, path);
            appl2 = txtSerialize.deserialize(path);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        System.out.println(appl2.getFirstName() + " " + appl2.getLastName() + " " + appl2.getSurname());
        System.out.println(appl2.getPassportCode() + " " + appl2.getCertificateCode());
        System.out.println("EXAM");
        System.out.println(appl2.getExam().getFirstExam() + " " + appl2.getExam()
                .getSecondExam() + " " + appl2.getExam().getThirdExam());
        System.out.println("SPECIALITY");
        System.out.println(appl2.getSpeciality().getUniversity() + " " + appl2.getSpeciality()
                .getFaculty() + " " + appl2.getSpeciality().getSpeciality());
        System.out.println(appl2.getAverangeMarkOfTheCertificate() + " " + appl2.getRuralCoef());
    }
}
