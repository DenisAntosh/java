package model;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.regex.Pattern;

@XmlRootElement
public class Applicant implements Serializable {
    @XmlElement
    private String firstName;
    @XmlElement
    private String lastName;
    @XmlElement
    private String surname;
    @XmlElement
    private String passportCode;
    @XmlElement
    private String certificateCode;
    @XmlElement
    private Exams exam;
    @XmlElement
    private Speciality speciality;
    @XmlElement
    private double averangeMarkOfTheCertificate;
    @XmlElement
    private boolean ruralCoef;

    private final static String NAME_PATTERN = "^[A-Z][a-z -]+?$";
    private final static String CODE_PATTERN = "^[A-Z]{2}\\d{6}$";
    private final static int MIN_SCORE_EXAM = 100;
    private final static int MAX_SCORE_EXAM = 200;
    private final static double MIN_SCORE_CERTIFICATE = 2.0;
    private final static double MAX_SCORE_CERTIFICATE = 12.0;

    public static class Builder {
        private Applicant applicantToBuild;

        public Builder() {
            applicantToBuild = new Applicant();
        }

        private Applicant build() throws IllegalArgumentException {
            Applicant builtPerson = applicantToBuild;
            applicantToBuild = null;
            return builtPerson;
        }

        public Applicant buildApplicant(String[] args) throws IllegalArgumentException {
            StringBuilder sb = new StringBuilder();

            try {
                this.setFirstName(args[0]);
            } catch (IllegalArgumentException ex) {
                sb.append(ex.toString()).append("\n");
            }

            try {
                this.setLastName(args[1]);
            } catch (IllegalArgumentException ex) {
                sb.append(ex.toString()).append("\n");
            }

            try {
                this.setSurname(args[2]);
            } catch (IllegalArgumentException ex) {
                sb.append(ex.toString()).append("\n");
            }

            try {
                this.setPassportCode(args[3]);
            } catch (IllegalArgumentException ex) {
                sb.append(ex.toString()).append("\n");
            }

            try {
                this.setCertificateCode(args[4]);
            } catch (IllegalArgumentException ex) {
                sb.append(ex.toString()).append("\n");
            }

            try {
                this.setExams(new Exams(Integer.parseInt(args[5]), Integer.parseInt(args[6]), Integer.parseInt(args[7])));
            } catch (IllegalArgumentException ex) {
                sb.append(ex.toString()).append("\n");
            }

            this.setSpeciality(new Speciality(args[8], args[9], args[10]));

            try {
                this.setAverangeMarkOfTheCertificate(Double.parseDouble(args[11]));
            } catch (IllegalArgumentException ex) {
                sb.append(ex.toString());
            }

            this.setRuralCoef(Boolean.getBoolean(args[12]));

            if (sb.length() == 0)
                return this.build();
            throw new IllegalArgumentException(sb.toString());
        }

        public Builder setFirstName(String name) {
            if (Pattern.matches(NAME_PATTERN, name))
                this.applicantToBuild.firstName = name;
            else
                throw new IllegalArgumentException("First name is incorrect");
            return this;
        }

        public Builder setLastName(String name) {
            if (Pattern.matches(NAME_PATTERN, name))
                this.applicantToBuild.lastName = name;
            else
                throw new IllegalArgumentException("Last name is incorrect");
            return this;
        }

        public Builder setSurname(String name) {
            if (Pattern.matches(NAME_PATTERN, name))
                this.applicantToBuild.surname = name;
            else
                throw new IllegalArgumentException("Surname is incorrect");
            return this;
        }

        public Builder setPassportCode(String passportCode) {
            if (Pattern.matches(CODE_PATTERN, passportCode))
                this.applicantToBuild.passportCode = passportCode;
            else
                throw new IllegalArgumentException("Passport code is incorrect");
            return this;
        }

        public Builder setCertificateCode(String certificateCode) {
            if (Pattern.matches(CODE_PATTERN, certificateCode))
                this.applicantToBuild.certificateCode = certificateCode;
            else
                throw new IllegalArgumentException("Certificate code is incorrect");
            return this;
        }

        public Builder setExams(Exams exam) {
            if (exam.getFirstExam() >= Applicant.MIN_SCORE_EXAM && exam.getFirstExam() <= MAX_SCORE_EXAM && exam.getSecondExam() >= MIN_SCORE_EXAM && exam
                    .getSecondExam() <= MAX_SCORE_EXAM && exam.getThirdExam() >= MIN_SCORE_EXAM && exam.getThirdExam() <= MAX_SCORE_EXAM)
                this.applicantToBuild.exam = exam;
            else
                throw new IllegalArgumentException("The exams must be bigger then 100 and less then 200");
            return this;
        }

        public Builder setSpeciality(Speciality speciality) {
            this.applicantToBuild.speciality = speciality;
            return this;
        }

        public Builder setAverangeMarkOfTheCertificate(double averangeMarkOfTheCertificate) {
            if (averangeMarkOfTheCertificate >= MIN_SCORE_CERTIFICATE && averangeMarkOfTheCertificate <= MAX_SCORE_CERTIFICATE)
                this.applicantToBuild.averangeMarkOfTheCertificate = averangeMarkOfTheCertificate;
            else
                throw new IllegalArgumentException("Averange mark must be in the range [2.0 - 12.0]");
            return this;
        }

        public Builder setRuralCoef(Boolean ruralCoef) {
            this.applicantToBuild.ruralCoef = ruralCoef;
            return this;
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSurname() {
        return surname;
    }

    public String getPassportCode() {
        return passportCode;
    }

    public String getCertificateCode() {
        return certificateCode;
    }

    public Exams getExam() {
        return exam;
    }

    public Speciality getSpeciality() {
        return speciality;
    }

    public double getAverangeMarkOfTheCertificate() {
        return averangeMarkOfTheCertificate;
    }

    public boolean getRuralCoef() {
        return ruralCoef;
    }

    private Applicant() {
        firstName = "";
        lastName = "";
        surname = "";
        passportCode = "";
        certificateCode = "";
        exam = new Exams();
        speciality = new Speciality("", "", "");
        averangeMarkOfTheCertificate = 0;
        ruralCoef = false;
    }

    private Applicant(String firstName, String lastName, String surname, String passportCode, String certificateCode,
                      Exams exam, Speciality speciality, double averangeMarkOfTheCertificate, boolean ruralCoef) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.surname = surname;
        this.passportCode = passportCode;
        this.certificateCode = certificateCode;
        this.exam = exam;
        this.speciality = speciality;
        this.averangeMarkOfTheCertificate = averangeMarkOfTheCertificate;
        this.ruralCoef = ruralCoef;
    }

    public static Applicant createApplicant() {
        return new Applicant();
    }

    /**
     * @return the finaly mark of the applicant
     */
    double getFinalMark() {
        return exam.getIntroductoryAssessment(averangeMarkOfTheCertificate, ruralCoef, speciality.getSpeciality());
    }

    /**
     * @param obj - the object with which we will compare with our object
     * @return true if the object are equals, false if not.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (getClass() != obj.getClass()))
            return false;
        Applicant other = (Applicant) obj;
        if (firstName != other.getFirstName() || lastName != other.getLastName() || surname != other.getSurname() || passportCode != other
                .getPassportCode())
            return false;
        return true;
    }

    /**
     * @return hashCode of the name, passport and certificate in the amount
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result + ((surname == null) ? 0 : surname.hashCode());
        result = prime * result + ((passportCode == null) ? 0 : passportCode.hashCode());
        return result;
    }

    /**
     * @return string result of converting all ZNO into one string
     */
    @Override
    public String toString() {
        String str = firstName + " " + lastName + " " + surname + " ";
        str += passportCode + " " + certificateCode + " ";
        str += exam.getFirstExam() + " " + exam.getSecondExam() + " " + exam.getThirdExam() + " ";
        str += speciality.getUniversity() + " " + speciality.getFaculty() + " " + speciality.getSpeciality() + " ";
        str += averangeMarkOfTheCertificate + " " + ruralCoef;
        return str;
    }
}