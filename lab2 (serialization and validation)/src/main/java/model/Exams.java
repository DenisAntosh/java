package model;

import java.util.*;

public class Exams {

    private int firstExam;
    private int secondExam;
    private int thirdExam;

    private static final double RURALCOEF = 1.02;
    private static final double AGRICULTURALCOEF = 1.05;
    private static final double FIRSTCOEF = 0.25;
    private static final double SECONDCOEF = 0.35;
    private static final double THIRDCOEF = 0.25;
    private static final double CEFTIFICATECOEF = 0.1;

    private static final List<String> AGRICULTURALSCIENCE = Arrays.asList("Агрономія", "Біологія", "Біотехнології та біоінженерія", "Геодезія та землеустрій", "Екологія", "Середня освіта (біологія та здоров'я людини)", "Середня освіта (хім0ія)", "Харчові технології", "Хімія", "Видавництво та поліг    рафія", "Електроенергетика, електротехніка та електромеханіка", "Метрологія та інформаційно-вимірювальна техніка", "Мікро- та наносистемна техніка", "Прикладна фізика та наноматеріали", "Професійна освіта (машинобудування)", "Середня освіта (трудове навчання та технології)", "Середня освіта (фізика)", "Телекомунікації та радіотехніка", "Будівництво та цивільна інженерія", "Географія", "Геодезія та землеустрій", "Науки про Землю", "Середня освіта (географія)", "Математика", "Середня освіта (інформатика)", "Середня освіта (математика)", "Системний аналіз");

    public Exams(int first, int second, int third) {
        if (first >= 100 && first <= 200 && second >= 100 && second <= 200 && third >= 100 && third <= 200) {
            firstExam = first;
            secondExam = second;
            thirdExam = third;
        }
        else
            throw new IllegalArgumentException("The exams must be bigger then 100 and less then 200");
    }

    public Exams() {
        firstExam = 0;
        secondExam = 0;
        thirdExam = 0;
    }

    public int getFirstExam() {
        return firstExam;
    }

    public int getSecondExam() {
        return secondExam;
    }

    public int getThirdExam() {
        return thirdExam;
    }

    public void setFirstExam(int firstExam) {
        if (firstExam >= 100 && firstExam <= 200)
            this.firstExam = firstExam;
        else
            throw new IllegalArgumentException("The exams must be bigger then 100 and less then 200");
    }

    public void setSecondExam(int secondExam) {
        if (secondExam >= 100 && secondExam <= 200)
            this.secondExam = secondExam;
        else
            throw new IllegalArgumentException("The exams must be bigger then 100 and less then 200");
    }

    public void setThirdExam(int thirdExam) {
        if (thirdExam >= 100 && thirdExam <= 200)
            this.thirdExam = thirdExam;
        else
            throw new IllegalArgumentException("The exams must be bigger then 100 and less then 200");
    }

    public double getIntroductoryAssessment(double averangeMarkOfTheCertificate, boolean ruralCoef, String speciality) {
        assert (averangeMarkOfTheCertificate > 0 && averangeMarkOfTheCertificate <= 12);
        double res = firstExam * FIRSTCOEF + secondExam * SECONDCOEF + thirdExam * THIRDCOEF + convertTo200PointScale(averangeMarkOfTheCertificate) * CEFTIFICATECOEF;
        if (ruralCoef)
            res *= RURALCOEF;
        if (AGRICULTURALSCIENCE.contains(speciality))
            res *= AGRICULTURALCOEF;
        return res;
    }

    private static double convertTo200PointScale(double number) {
        assert (number <= 12);
        number += 8;
        number *= 10;
        return number;
    }
};