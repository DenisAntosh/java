package model;

public class Applicant {
    private String firstName;
    private String lastName;
    private String surname;
    private String passportCode;
    private String certificateCode;
    private Exams exam;
    private Speciality speciality;
    private double averangeMarkOfTheCertificate;
    private boolean ruralCoef;

    public String getFirstName() { return firstName; }
    public String getLastName() { return lastName; }
    public String getSurname() { return surname; }
    public String getPassportCode() { return passportCode; }
    public String getCertificateCode() { return certificateCode; }
    public Exams getExams() { return exam; }
    public Speciality getSpeciality() { return speciality; }
    public double getAverangeMarkOfTheCertificate() { return averangeMarkOfTheCertificate; }
    public boolean getRuralCoef() { return ruralCoef; }

    public void setFirstName(String firstName) { this.firstName = firstName; }
    public void setLastName(String lastName) { this.lastName = lastName; }
    public void setSurname(String surname) { this.surname = surname; }
    public void setPassportCode(String passportCode) { this.passportCode = passportCode; }
    public void setCertificateCode(String certificateCode) { this.certificateCode = certificateCode; }
    public void setExam(Exams exam) { this.exam = exam; }
    public void setSpeciality(Speciality speciality) { this.speciality = speciality; }
    public void setAverangeMarkOfTheCertificate(double averangeMarkOfTheCertificate) { this.averangeMarkOfTheCertificate = averangeMarkOfTheCertificate; }
    public void setRuralCoef(boolean ruralCoef) { this.ruralCoef = ruralCoef; }

    Applicant(String firstName, String lastName, String surname, String passport, String certificate, Exams exam, Speciality spec, double avgMark, boolean haveRuralCoef) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.surname = surname;
        passportCode = passport;
        certificateCode = certificate;
        this.exam = exam;
        speciality = spec;
        averangeMarkOfTheCertificate = avgMark;
        ruralCoef = haveRuralCoef;
    }

    /**
     * @return the finaly mark of the applicant
     */
    double getFinalMark()
    {
        return exam.getIntroductoryAssessment(averangeMarkOfTheCertificate, ruralCoef, speciality.getSpeciality());
    }

    /**
     * @param obj - the object with which we will compare with our object
     * @return true if the object are equals, false if not.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (getClass() != obj.getClass()))
            return false;
        Applicant other = (Applicant) obj;
        if (firstName != other.getFirstName() || lastName != other.getLastName() || surname != other.getSurname() || passportCode != other.getPassportCode())
            return false;
        return true;
    }

    /**
     * @return hashCode of the name, passport and certificate in the amount
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result + ((surname == null) ? 0 : surname.hashCode());
        result = prime * result + ((passportCode == null) ? 0 : passportCode.hashCode());
        return result;
    }

    /**
     * @return string result of converting all ZNO into one string
     */
    @Override
    public String toString()
    {
        String str = Integer.toString(exam.getFirstExam()) + " " + Integer.toString(exam.getSecondExam()) + " " + Integer.toString(exam.getThirdExam());
        return str;
    }
};