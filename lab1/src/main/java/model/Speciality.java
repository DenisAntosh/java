package model;

public class Speciality
{
    private String university;
    private String faculty;
    private String speciality;

    Speciality(String university, String faculty, String speciality)
    {
        this.university = university;
        this.faculty = faculty;
        this.speciality = speciality;
    }

    public String getFaculty() { return faculty; }
    public String getUniversity() { return university; }
    public String getSpeciality() { return speciality; }
    public void setFaculty(String faculty) { this.faculty = faculty; }
    public void setSpeciality(String speciality) { this.speciality = speciality; }
    public void setUniversity(String university) { this.university = university; }
}
