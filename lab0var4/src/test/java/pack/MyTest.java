package pack;
import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;
import org.testng.annotations.DataProvider;;

public class MyTest
{
    public static double EPS = 0.0000001;
    @Test(dataProvider = "beginProvider")
    public void beginTest(double d, double rez)
    {
        assertEquals(new Main().beginTask(d), rez, EPS);
    }

    @DataProvider
    public Object[][] beginProvider()
    {
        return new Object[][]{{5, 15.7}, {1, 3.14}};
    }

    @Test(expectedExceptions = AssertionError.class)
    public void negativeInputTest() {
        new Main().beginTask(-2);
    }

    //////////////////////////////////////////////////////////////////

    @Test(dataProvider = "integerProvider")
    public void integerTest(int a, int b, int res)
    {
        assertEquals(new Main().integerNumbersTask(a,b),  res);
    }

    @DataProvider
    public Object[][] integerProvider()
    {
        return new Object[][] { {12, 3, 4}, {22, 7, 3 }, {31,5, 6}};
    }

    @Test(expectedExceptions = AssertionError.class)
    public void negativIntegerTest()
    {
        new Main().integerNumbersTask(41, -8);
    }

    ///////////////////////////////////////////////////////////////////////////////////////

    @Test(dataProvider = "booleanProvider")
    public void booleanTest(int a, int b, boolean res)
    {
        assertEquals(new Main().booleanTask(a, b), res);
    }

    @DataProvider
    public Object[][] booleanProvider()
    {
        return new Object[][] { {3, 2, true}, {3, 3, true}, {2, 3, false} };
    }

    //////////////////////////////////////////////////////////////////////////////////////

    @Test(dataProvider = "ifProvider")
    public void ifTest(int a, int b, int c, int res)
    {
        assertEquals(new Main().ifTask(a,b,c), res);
    }

    @DataProvider
    public Object[][] ifProvider()
    {
        return new Object[][] { {-1, -2, -3, 0}, {1, 2, -3, 2}, { 10, 15, 17, 3} };
    }

    //////////////////////////////////////////////////////////////////////////////////////

    @Test(dataProvider = "switchProvider")
    public void switchTest(int num, int res)
    {
        assertEquals(new Main().switchTask(num), res);
    }

    @DataProvider
    public Object[][] switchProvider()
    {
        return new Object[][] { {7, 31}, {2, 28}, {6, 30} };
    }

    @Test(expectedExceptions = AssertionError.class)
    public void rangeSwitchTest()
    {
        new Main().switchTask(15);
    }

    //////////////////////////////////////////////////////////////////////////////////////

    @Test(dataProvider = "forProvider")
    public void forTest(int x, int n, double res)
    {
        assertEquals(new Main().forTask(x, n), res);
    }

    @DataProvider
    public Object[][] forProvider()
    {
        return new Object[][] { {-2, 5, 1.8444444444444446}, {10, 11, 1722.9305947683733}};
    }

    @Test(expectedExceptions = AssertionError.class)
    public void negativeInputForTest()
    {
        new Main().forTask(3, -2);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////

    @Test (dataProvider = "whileProvider")
    public void whileTest(int n, boolean res)
    {
        assertEquals(new Main().whileTask(n), res);
    }

    @DataProvider
    public Object[][] whileProvider()
    {
        return new Object[][] { {18, false}, {22, false}, {35, false}, {27,true} };
    }

    @Test(expectedExceptions = AssertionError.class)
    public void negativeInputWhileTest()
    {
        new Main().whileTask(-1);
    }

    /////////////////////////////////////////////////////////////////////////

    @Test (dataProvider = "arrayProvider")
    public void arrayTest(double[] array, int k, int l, double res)
    {
        assertEquals(new Main().arrayTask(array, k, l), res);
    }

    @DataProvider
    public Object[][] arrayProvider()
    {
        return new Object[][] { { new double[]{5.2, 7.4, 1, 2, 6.9, 24, 2},  2, 5, 8.475},
                                { new double[]{12, 5.6, 4, 2, 22.7, 8.4, 3}, 0, 2, 7.2 } };
    }

    @Test(expectedExceptions = AssertionError.class)
    public void badRangeArrayTest()
    {
        new Main().arrayTask(new double[]{5.2, 7.4, 1, 2, 6.9, 24, 2}, 3, 2 );
    }

    //////////////////////////////////////////////////////////////////////////////

    @Test(dataProvider = "matrixProvider")
    public void twoDimensionArrayTest(int[][] input, int[][] output)
    {
        assertEquals(new Main().twoDimensionArrayTask(input), output);
    }

    @DataProvider
    public Object[][] matrixProvider() {
        int[][] input1 = {  {2, 3, 6, -9, 9},
                            {34, -9, 98, 2, 1},
                            {6, 2, 1, -4, 5},
                            {8, -98, 2, 5, 3}
                        };

        int[][] out1 = {    {34,  -98, 6, 5, 1 },
                            {2, -9, 1, 2, 9 },
                            {6, 2, 98, -4, 5 },
                            {8, 3, 2, -9, 3 } };

        int[][] input2 = {  {-98, 8, 1, 5, 3},
                            {-4, 2, 4, 6, 1},
                            {34, 98, -9, 2, -4},
                            {2, 3, 6, 9, -9}    };

        int[][] out2 = {    {34, 8, 1, 5, -9},
                            {-4, 98, 4, 6, 1},
                            {-98, 2, 6, 9, -4},
                            {2, 3, -9, 2, 3}    };

        return new Object[][]{{input1, out1}, {input2, out2}};
    }
}